package ru.kazakov.iteco.entity;

public class Task {
    private String name;
    private String taskInfo;

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getTaskInfo() {
        return taskInfo;
    }

    public void setTaskInfo(String taskInfo) {
        this.taskInfo = taskInfo;
    }
}
