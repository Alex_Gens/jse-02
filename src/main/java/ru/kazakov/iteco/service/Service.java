package ru.kazakov.iteco.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Service {
   public static final String separator = System.lineSeparator();
   public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

   public static String enterName() throws IOException {
        String name = reader.readLine();
        while (name.length() == 0) {
            name = reader.readLine();
        }
        return name.trim();
    }

   public static void help() {
        System.out.print("help: Show all commands." + separator +
                "project-create: Create new project." + separator +
                "project-get: Show all project information." + separator +
                "project-update: Update project information." + separator +
                "project-remove: Remove project." + separator +
                "project-list: Show all projects." + separator +
                "project-clear: Remove all projects." + separator +
                "task-create: Create new task." + separator +
                "task-get: Show all task information." + separator +
                "task-update: Update task information." + separator +
                "task-remove: Remove task." + separator +
                "task-list: Show all tasks." + separator +
                "task-clear: Remove all tasks." + separator +
                "-save: Save all entered information to project or task." + separator +
                "manager-close: Close task manager.");
        System.out.print(separator);
        System.out.print(separator);
    }
}
