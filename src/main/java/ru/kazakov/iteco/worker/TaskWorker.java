package ru.kazakov.iteco.worker;

import ru.kazakov.iteco.entity.Task;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import static ru.kazakov.iteco.service.Service.*;

public class TaskWorker {
    LinkedHashMap<String, Task> tasks = new LinkedHashMap<>();

    public void createTask() throws IOException {
        System.out.println("ENTER TASK NAME:");
        String taskName = enterName();
        if (tasks.containsKey(taskName)) {
            System.out.println("[NOT CREATED]" + separator + "Task with this name is already exists. Use another task name.");
        } else {
            tasks.put(taskName, new Task(taskName));
            System.out.println("[CREATED]" + separator + "Task successfully created!");
        }
        System.out.print(separator);
    }

    public void getTask() throws IOException {
        System.out.println("ENTER TASK NAME:");
        String taskName = enterName();
        if (tasks.containsKey(taskName)) {
            if (tasks.get(taskName).getTaskInfo() == null || tasks.get(taskName).getTaskInfo().length() == 0) {
                System.out.println("Task is empty. Use \"task-update\" to update this task.");
            }
            System.out.println("Task: " + taskName);
            System.out.println(tasks.get(taskName).getTaskInfo());
        } else {
            System.out.println("Tasks with this name doesn't exist. Use \"task-list\" to show all tasks.");
        }
        System.out.print(separator);
    }

    public void updateTask() throws IOException {
        System.out.println("ENTER TASK NAME:");
        String taskName = enterName();
        if (tasks.containsKey(taskName)) {
            System.out.println("Use \"-save\" to finish entering, and save information.");
            System.out.println("ENTER TASK INFORMATION");
            StringBuilder builder = new StringBuilder();
            String temp = "";
            while (true) {
                temp = reader.readLine().trim();
                if (temp.equals("-save")) {
                    break;
                }
                builder.append(temp);
                builder.append(separator);
            }
            tasks.get(taskName).setTaskInfo(builder.toString());
            System.out.println("[UPDATED]" + separator + "Task successfully updated!");
        } else {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
        }
        System.out.print(separator);
    }

    public void removeTask() throws IOException {
        System.out.println("ENTER TASK NAME:");
        String taskName = enterName();
        if (tasks.containsKey(taskName)) {
            tasks.remove(taskName);
            System.out.println("[REMOVED]" + separator + "Task successfully removed!");
        } else {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
        }
        System.out.print(separator);
    }

    public void listTask() {
        if (tasks.isEmpty()) {
            System.out.println("Tasks list is empty. Use \"task-create\" to create task.");
        } else {
            int counter = 1;
            System.out.println("[TASK LIST]");
            for (Map.Entry<String, Task> entry : tasks.entrySet()
            ) {
                System.out.println(counter + ". " + entry.getKey());
                counter++;
            }
        }
        System.out.print(separator);
    }

    public void clearTask() {
        tasks.clear();
        System.out.println("[ALL TASKS REMOVED]" + separator + "Tasks successfully removed!" + separator);
    }
}
