package ru.kazakov.iteco.worker;

import ru.kazakov.iteco.entity.Project;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import static ru.kazakov.iteco.service.Service.*;

public class ProjectWorker {
     LinkedHashMap<String, Project> projects = new LinkedHashMap<>();

    public void createProject() throws IOException {
        System.out.println("ENTER PROJECT NAME:");
        String projectName = enterName();
        if (projects.containsKey(projectName)) {
            System.out.println("[NOT CREATED]" + separator + "Project with this name is already exists. Use another project name.");
        } else {
            projects.put(projectName, new Project(projectName));
            System.out.println("[CREATED]" + separator + "Project successfully created!");
        }
        System.out.print(separator);
    }

    public void getProject() throws IOException {
        System.out.println("ENTER PROJECT NAME:");
        String projectName = enterName();
        if (projects.containsKey(projectName)) {
            if (projects.get(projectName).getProjectInfo() == null || projects.get(projectName).getProjectInfo().length() == 0) {
                System.out.println("Project is empty. Use \"project-update\" to update this project.");
            }
            System.out.println("[PROJECT: " + projectName + "]");
            System.out.println(projects.get(projectName).getProjectInfo());
        } else {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
        }
        System.out.print(separator);
    }

    public void updateProject() throws IOException {
        System.out.println("ENTER PROJECT NAME:");
        String projectName = enterName();
        if (projects.containsKey(projectName)) {
            System.out.println("Use \"-save\" to finish entering, and save information.");
            System.out.println("ENTER PROJECT INFORMATION");
            StringBuilder builder = new StringBuilder();
            String temp = "";
            while (true){
                temp = reader.readLine().trim();
                if (temp.equals("-save")) {
                    break;
                }
                builder.append(temp);
                builder.append(separator);
            }
            projects.get(projectName).setProjectInfo(builder.toString());
            System.out.println("[UPDATED]" + separator + "Project successfully updated!");
        } else {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
        }
        System.out.print(separator);
    }

    public void removeProject() throws IOException {
        System.out.println("ENTER PROJECT NAME:");
        String projectName = enterName();
        if (projects.containsKey(projectName)) {
            projects.remove(projectName);
            System.out.println("[REMOVED]" + separator + "Project successfully removed!");
        } else {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
        }
        System.out.print(separator);
    }

    public void listProject() {
        if (projects.isEmpty()) {
            System.out.println("Project list is empty. Use \"project-create\" to create project.");
        } else {
            int counter = 1;
            System.out.println("[PROJECTS LIST]");
            for (Map.Entry<String, Project> entry : projects.entrySet()
            ) {
                System.out.println(counter + ". " + entry.getKey());
                counter++;
            }
        }
        System.out.print(separator);
    }

    public void clearProject() {
        projects.clear();
        System.out.println("[ALL PROJECTS REMOVED]" + separator + "Projects successfully removed!" + separator);
    }

}
