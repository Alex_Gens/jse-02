package ru.kazakov.iteco;

import ru.kazakov.iteco.worker.ProjectWorker;
import ru.kazakov.iteco.worker.TaskWorker;
import static ru.kazakov.iteco.service.Service.*;
import java.io.IOException;

public class TaskManager {

    public static void main(String[] args) throws IOException {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        ProjectWorker projectWorker = new ProjectWorker();
        TaskWorker taskWorker = new TaskWorker();
        String command = enterName().trim().toLowerCase();
        while (!command.equals("manager-close")) {
            switch (command) {
                case "help"           : help();                        break;
                case "project-create" : projectWorker.createProject(); break;
                case "project-get"    : projectWorker.getProject();    break;
                case "project-update" : projectWorker.updateProject(); break;
                case "project-remove" : projectWorker.removeProject(); break;
                case "project-list"   : projectWorker.listProject();   break;
                case "project-clear"  : projectWorker.clearProject();  break;
                case "task-create"    : taskWorker.createTask();       break;
                case "task-get"       : taskWorker.getTask();          break;
                case "task-update"    : taskWorker.updateTask();       break;
                case "task-remove"    : taskWorker.removeTask();       break;
                case "task-list"      : taskWorker.listTask();         break;
                case "task-clear"     : taskWorker.clearTask();        break;
                default               :
                    System.out.println("Command doesn't exist. Use \"help\" to show all commands." + separator);
            }
            command = enterName().trim().toLowerCase();
        }
        System.out.println("*** MANAGER CLOSED ***");
    }





}
